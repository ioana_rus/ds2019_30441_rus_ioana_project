package com.example.assignment.events;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class MessageRecievedEvent extends BaseEvent {
    private final String message;
    public MessageRecievedEvent(String message){
        super(EventType.MESSAGE_RECIEVED);
        this.message = message;
    }
}
