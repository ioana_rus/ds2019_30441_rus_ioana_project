package com.example.assignment.validators;

import com.example.assignment.entities.Activity;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import java.time.temporal.ChronoUnit;

@Component
public class ActivityValidator {
    private static final Log LOGGER = LogFactory.getLog(ActivityValidator.class);
    public  String validateActivity(Activity activity){
        String message = "";
        if(activity.getName().equals("Sleeping")){
            long hours = ChronoUnit.HOURS.between(activity.getStart(), activity.getEnd());
            if(hours >= 12){
                message = "Patient with id " + activity.getPatient().getPatientId() + " slept for more than 12 hours";
            }
        }
        if(activity.getName().equals("Leaving")){
            long hours = ChronoUnit.HOURS.between(activity.getStart(), activity.getEnd());
            if(hours >= 12){
                message = "Patient with id " + activity.getPatient().getPatientId() + " was outside for more than 12 hours";
            }
        }
        if(activity.getName().equals("Toileting")){
            long hours = ChronoUnit.HOURS.between(activity.getStart(), activity.getEnd());
            if(hours >= 1){
                message = "Patient with id " + activity.getPatient().getPatientId() + " was in the bathroom for more than 1 hour";
            }
        }
        return message;
    }
}
