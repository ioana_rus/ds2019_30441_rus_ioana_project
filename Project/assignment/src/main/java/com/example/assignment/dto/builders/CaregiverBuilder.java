package com.example.assignment.dto.builders;

import com.example.assignment.dto.CaregiverDTO;
import com.example.assignment.entities.Caregiver;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class CaregiverBuilder {
    public static Caregiver getEntityFromDTO(CaregiverDTO dto){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate birthDate = LocalDate.parse(dto.getBirthDate(), formatter);
        return new Caregiver(dto.getCaregiverId(),dto.getName(),dto.getGender(),birthDate,dto.getAddress(),dto.getUsername(),dto.getPassword());
    }
}
