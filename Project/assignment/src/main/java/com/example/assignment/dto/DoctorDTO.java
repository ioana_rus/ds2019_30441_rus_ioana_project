package com.example.assignment.dto;

import com.example.assignment.entities.Doctor;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DoctorDTO {
    private Integer doctorId;
    private String name;
    private String gender;
    private String birthDate;
    private String address;
    private String username;
    private String password;

    public static DoctorDTO ofEntity(Doctor doctor){
        DoctorDTO dto = new DoctorDTO();
        dto.setDoctorId(doctor.getDoctorId());
        dto.setName(doctor.getName());
        dto.setGender(doctor.getGender());
        dto.setBirthDate(doctor.getBirthDate().toString());
        dto.setAddress(doctor.getAddress());
        dto.setUsername(doctor.getUsername());
        dto.setPassword(doctor.getPassword());
        return dto;
    }
}
