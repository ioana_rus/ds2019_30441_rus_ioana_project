package com.example.assignment.controllers;


import com.example.assignment.dto.PatientDTO;
import com.example.assignment.services.PatientService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class PatientController {
    private final PatientService patientService;

    @GetMapping("/patients/{id}")
    public PatientDTO findPatientById(@PathVariable Integer id){
        return patientService.findPatientById(id);
    }

    @GetMapping("/patients")
    public List<PatientDTO> findAllPatients(){
        return patientService.findAllPatients();
    }

    @PostMapping("/patients")
    public Integer insertPatient(@RequestBody PatientDTO patientDTO){
        return patientService.insertPatient(patientDTO);
    }

    @PutMapping("/patients")
    public Integer updatePatient(@RequestBody PatientDTO patientDTO){
        return patientService.updatePatient(patientDTO);
    }

    @DeleteMapping("/patients/{id}")
    public void deletePatient(@PathVariable Integer id){
        patientService.deletePatient(id);
    }
}
