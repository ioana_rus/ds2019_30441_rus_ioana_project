package com.example.assignment.repositories;

import com.example.assignment.entities.Activity;
import com.example.assignment.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActivityRepository extends JpaRepository<Activity, Integer> {
    List<Activity> findAllByPatient(Patient pantient);
}
