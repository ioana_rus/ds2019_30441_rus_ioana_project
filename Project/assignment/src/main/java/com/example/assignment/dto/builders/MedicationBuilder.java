package com.example.assignment.dto.builders;

import com.example.assignment.dto.MedicationDTO;
import com.example.assignment.entities.Medication;

public class MedicationBuilder {
    public static Medication getEntityFromDTO(MedicationDTO dto){
        return new Medication(dto.getMedicationId(),dto.getName(),dto.getSideEffect());
    }
}
