package com.example.assignment.controllers;

import com.example.assignment.events.BaseEvent;
import com.example.assignment.services.ActivityService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
public class MessageController {
    private final ActivityService activityService;
    private final SimpMessagingTemplate messagingTemplate;

    @EventListener(BaseEvent.class)
    public void handleMessageRecieved(BaseEvent event){
        log.info("Got an event: {}.",event);
        messagingTemplate.convertAndSend("/topic/events",event);
    }

}
