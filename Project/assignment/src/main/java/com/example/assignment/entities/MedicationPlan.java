package com.example.assignment.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.time.LocalDateTime;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "medication_plan")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MedicationPlan {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "medication_plan_id", unique = true, nullable = false)
    private Integer medictionPlanId;
    @ManyToOne()
    @JoinColumn(name = "patient_id",  nullable = false)
    private Patient patient;
    @Column(name = "start_time", nullable = false)
    private LocalDateTime startTime;
    @Column(name = "end_time", nullable = false)
    private LocalDateTime endTime;
    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(name = "medication_to_plan", joinColumns = @JoinColumn(name = "medication_id"),
            inverseJoinColumns = @JoinColumn(name = "medication_plan_id"))
    private List<Medication> medication;
    @Column(name = "status",nullable = false)
    private String status;

    public MedicationPlan(Patient patient, LocalDateTime startTime, LocalDateTime endTime, List<Medication> medication){
        this.patient = patient;
        this.startTime = startTime;
        this.endTime = endTime;
        this.medication = medication;
    }
}
