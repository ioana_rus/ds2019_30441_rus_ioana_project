package com.example.assignment.dto;

import com.example.assignment.entities.Medication;
import com.example.assignment.entities.MedicationPlan;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MedicationPlanDTO implements Serializable {
    private Integer id;
    private Integer patientId;
    private String startTime;
    private String endTime;
    private List<String> medication;
    private String status;

    public static MedicationPlanDTO ofEntity(MedicationPlan medicationPlan){
        List<String> meds = new ArrayList<String>();
        for(Medication m: medicationPlan.getMedication()){
            meds.add(m.getName());
        }
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String startTime = medicationPlan.getStartTime().format(formatter);
        String endTime = medicationPlan.getEndTime().format(formatter);
        return new MedicationPlanDTO(medicationPlan.getMedictionPlanId(),medicationPlan.getPatient().getPatientId(),
                startTime,endTime,meds,medicationPlan.getStatus());
    }
}
