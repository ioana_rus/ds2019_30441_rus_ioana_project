package com.example.assignment.validators;

import com.example.assignment.dto.PatientDTO;
import com.example.assignment.errorhandler.IncorrectParameterException;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class PatientFieldValidator {
    private static final Log LOGGER = LogFactory.getLog(PatientFieldValidator.class);

    public static void validateInsertOrUpdate(PatientDTO patientDTO){
        List<String> errors = new ArrayList<>();
        if(patientDTO == null){
            errors.add("patientDTO is null");
            throw new IncorrectParameterException(PatientDTO.class.getSimpleName(), errors);
        }
        if(!errors.isEmpty()){
            LOGGER.error(errors);
            throw new IncorrectParameterException(PatientFieldValidator.class.getSimpleName(), errors);
        }
    }

}