package com.example.assignment.services;

import com.example.assignment.dto.CaregiverDTO;
import com.example.assignment.dto.builders.CaregiverBuilder;
import com.example.assignment.entities.Caregiver;
import com.example.assignment.errorhandler.DuplicateEntryException;
import com.example.assignment.errorhandler.ResourceNotFoundException;
import com.example.assignment.repositories.CaregiverRepository;
import com.example.assignment.validators.CaregiverFieldValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CaregiverService {
    private final CaregiverRepository caregiverRepository;

    @Transactional
    public CaregiverDTO findCaregiverById(Integer id){
        Optional<Caregiver> caregiver = caregiverRepository.findById(id);
        if(!caregiver.isPresent()){
            throw new ResourceNotFoundException("Caregiver", "caregiver id", id);
        }
        return CaregiverDTO.ofEntity(caregiver.get());
    }

    @Transactional
    public List<CaregiverDTO> findAllCaregivers(){
        List<Caregiver> caregivers = caregiverRepository.findAllOrdered();
        return caregivers.stream().map(CaregiverDTO::ofEntity).collect(Collectors.toList());
    }

    @Transactional
    public Integer insertCaregiver(CaregiverDTO caregiverDTO){
        CaregiverFieldValidator.validateInsertOrUpdate(caregiverDTO);
        return caregiverRepository
                .save(CaregiverBuilder.getEntityFromDTO(caregiverDTO))
                .getCaregiverId();
    }

    @Transactional
    public Integer updateCaregiver(CaregiverDTO caregiverDTO){
        Optional<Caregiver> caregiver = caregiverRepository.findById(caregiverDTO.getCaregiverId());
        if(!caregiver.isPresent()){
            throw new ResourceNotFoundException("Caregiver", "caregiver id", caregiverDTO.getCaregiverId());
        }
        CaregiverFieldValidator.validateInsertOrUpdate(caregiverDTO);
        return caregiverRepository.save(CaregiverBuilder.getEntityFromDTO(caregiverDTO)).getCaregiverId();
    }

    @Transactional
    public void deleteCaregiver(Integer id){
        this.caregiverRepository.deleteById(id);
    }
}
