package com.example.assignment.services;

import com.example.assignment.dto.PatientDTO;
import com.example.assignment.dto.builders.PatientBuilder;
import com.example.assignment.entities.Patient;
import com.example.assignment.errorhandler.DuplicateEntryException;
import com.example.assignment.errorhandler.ResourceNotFoundException;
import com.example.assignment.repositories.PatientRepository;
import com.example.assignment.validators.PatientFieldValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PatientService {
    private final PatientRepository patientRepository;

    @Transactional
    public PatientDTO findPatientById(Integer id){
        Optional<Patient> patient = patientRepository.findById(id);
        if(!patient.isPresent()){
            throw new ResourceNotFoundException("Patient", "patient id", id);
        }
        return PatientDTO.ofEntity(patient.get());
    }

    @Transactional
    public List<PatientDTO> findAllPatients(){
        List<Patient> patients = patientRepository.findAllOrdered();
        return patients.stream().map(PatientDTO::ofEntity).collect(Collectors.toList());
    }

    @Transactional
    public Integer insertPatient(PatientDTO patientDTO){
        PatientFieldValidator.validateInsertOrUpdate(patientDTO);
        return patientRepository
                .save(PatientBuilder.getEntityFromDTO(patientDTO))
                .getPatientId();
    }



    @Transactional
    public Integer updatePatient(PatientDTO patientDTO){
        Optional<Patient> patient = patientRepository.findById(patientDTO.getPatientId());
        if(!patient.isPresent()){
            throw new ResourceNotFoundException("Patient", "patient id", patientDTO.getPatientId());
        }
        PatientFieldValidator.validateInsertOrUpdate(patientDTO);
        return patientRepository.save(PatientBuilder.getEntityFromDTO(patientDTO)).getPatientId();
    }

    @Transactional
    public void deletePatient(Integer id){
        this.patientRepository.deleteById(id);
    }
}
