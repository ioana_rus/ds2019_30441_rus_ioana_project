package com.example.assignment.controllers;


import com.example.assignment.services.MedicationPlanService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class MedicationPlanController {
    private final MedicationPlanService medicationPlanService;

    @PutMapping("/medicationPlan/{id}")
    public Integer updateMedication(@PathVariable Integer id, @RequestParam(value="name") String name){
        return medicationPlanService.updateMedicationPlan(id,name);
    }
}
