package com.example.assignment.dto.builders;

import com.example.assignment.dto.DoctorDTO;
import com.example.assignment.entities.Doctor;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DoctorBuilder {

    public static Doctor getEntityFromDTO(DoctorDTO dto){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate birthDate = LocalDate.parse(dto.getBirthDate(), formatter);
        return new Doctor(dto.getDoctorId(),dto.getName(),dto.getGender(),birthDate,dto.getAddress(),dto.getUsername(),dto.getPassword());
    }
}
