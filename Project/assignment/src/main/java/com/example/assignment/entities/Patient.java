package com.example.assignment.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.time.LocalDate;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "patient")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Patient {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "patient_id", unique = true, nullable = false)
    private Integer patientId;
    @Column(name = "name", nullable = false, length = 100)
    private String name;
    @Column(name = "gender", nullable = false, length = 50)
    private String gender;
    @Column(name = "birthdate", nullable = false)
    private LocalDate birthDate;
    @Column(name = "address", nullable = false, length = 200)
    private String address;
    @Column(name = "username", nullable = false)
    private String username;
    @Column(name = "password", nullable = false, length = 500)
    private String password;
    @ManyToOne()
    @JoinColumn(name = "doctor_id",  nullable = false)
    private Doctor doctor;
    @ManyToOne()
    @JoinColumn(name = "caregiver_id", nullable = false)
    private Caregiver caregiver;
    @Column(name = "meidcal_record", length = 300)
    private String medicalRecord;
}
