package com.example.assignment.webservice;

import com.example.assignment.entities.ActivityData;
import com.example.assignment.services.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.jws.WebService;
import java.util.List;

@Component
@WebService(serviceName = "ActivityWebServiceImplService",endpointInterface = "com.example.assignment.webservice.ActivityWebService")
public class ActivityWebServiceImpl implements ActivityWebService{

    @Autowired
    private  ActivityService activityService;


    @Override
    public List<ActivityData> getActivities(Integer id) {
        return activityService.getActivities(id);
    }

    @Override
    public String checkPatientTookMedication(Integer patientId) {

        return activityService.checkPatientTookMedication(patientId);

    }

    @Override
    public List<String> checkActivityBehavior(Integer patientId) {
        return activityService.checkActivityBehavior(patientId);
    }

    @Override
    public void giveRecommendations(Integer patientId, String activity, String rec) {
        activityService.giveRecommendation(patientId,activity,rec);
    }


}
