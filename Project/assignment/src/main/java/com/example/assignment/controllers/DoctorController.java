package com.example.assignment.controllers;

import com.example.assignment.dto.DoctorDTO;
import com.example.assignment.services.DoctorService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
public class DoctorController {
    private final DoctorService doctorService;

    @GetMapping("/doctors/{id}")
    public DoctorDTO findDoctorById(@PathVariable Integer id){
        return doctorService.findDoctorById(id);
    }

    @PutMapping("/doctors")
    public Integer updateDoctor(@RequestBody DoctorDTO doctorDTO){
        return doctorService.updateDoctor(doctorDTO);
    }
}
