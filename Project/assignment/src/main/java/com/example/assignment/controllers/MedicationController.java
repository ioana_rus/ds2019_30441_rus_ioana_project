package com.example.assignment.controllers;

import com.example.assignment.dto.MedicationDTO;
import com.example.assignment.services.MedicationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class MedicationController {
    private final MedicationService medicationService;

    @GetMapping(value = "/medication", params = "name")
    public MedicationDTO findMedicationByName(@RequestParam("name") String name){
        return medicationService.findMedicationByName(name);
    }

    @GetMapping("/medication")
    public List<MedicationDTO> findAllMedications(){
        return medicationService.findAllMedications();
    }

    @PostMapping("/medication")
    public Integer insertMedication(@RequestBody MedicationDTO medicationDTO){
        return medicationService.insertMedication(medicationDTO);
    }

    @PutMapping("/medication")
    public Integer updateMedication(@RequestBody MedicationDTO medicationDTO){
        return medicationService.updateMedication(medicationDTO);
    }

    @DeleteMapping("/medication/{id}")
    public void deleteMedication(@PathVariable Integer id){
        medicationService.deleteMedication(id);
    }
}
