package com.example.assignment.repositories;

import com.example.assignment.entities.Medication;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MedicationRepository extends JpaRepository<Medication, Integer> {
    @Query(value = "SELECT u FROM Medication u ORDER BY u.name")
    List<Medication> findAllOrdered();

    Optional<Medication> findByName(String name);
}
