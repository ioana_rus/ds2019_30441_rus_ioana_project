package com.example.assignment.reciever;

import com.example.assignment.dto.ActivityDTO;
import com.example.assignment.services.ActivityService;
import lombok.RequiredArgsConstructor;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
@RequiredArgsConstructor
public class ActivityReciever {
    private final ActivityService activityService;
    @RabbitListener(queues = "queue")
    public void receive(byte[] data) {
        String consumedMessage = new String(data);
        System.out.println(" [x] Received '" + consumedMessage + "'");
        String[] fields=consumedMessage.split("\\s\t");
        Integer patientId=1;
        String activity = fields[2];
        String start = fields[0];
        String end = fields[1];
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime fromDate = LocalDateTime.parse(start, formatter);
        LocalDateTime toDate = LocalDateTime.parse(end,formatter);
        ActivityDTO patientActivity = new ActivityDTO(patientId,activity,start,end,"normal");
        activityService.addActivity(patientActivity);
    }
}
