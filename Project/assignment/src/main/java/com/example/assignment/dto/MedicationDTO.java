package com.example.assignment.dto;

import com.example.assignment.entities.Medication;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MedicationDTO {
    private Integer medicationId;
    private String name;
    private String sideEffect;

    public static MedicationDTO ofEntity(Medication medication){
        return new MedicationDTO(medication.getMeidcationId(),medication.getName(),medication.getSideEffect());
    }
}
