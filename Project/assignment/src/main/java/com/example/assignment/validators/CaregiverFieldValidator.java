package com.example.assignment.validators;

import com.example.assignment.dto.CaregiverDTO;
import com.example.assignment.errorhandler.IncorrectParameterException;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class CaregiverFieldValidator {
    private static final Log LOGGER = LogFactory.getLog(CaregiverFieldValidator.class);
    public static void validateInsertOrUpdate(CaregiverDTO caregiverDTO){
        List<String> errors = new ArrayList<>();
        if(caregiverDTO == null){
            errors.add("caregiverDTO is null");
            throw new IncorrectParameterException(CaregiverDTO.class.getSimpleName(), errors);
        }
        if(!errors.isEmpty()){
            throw new IncorrectParameterException(CaregiverFieldValidator.class.getSimpleName(), errors);
        }
    }
}