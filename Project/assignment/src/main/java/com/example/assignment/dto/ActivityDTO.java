package com.example.assignment.dto;

import com.example.assignment.entities.Activity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ActivityDTO {
    private Integer id;
    private Integer patienId;
    private String name;
    private String start;
    private String end;
    private String behavior;

    public ActivityDTO(Integer patienId,String name, String start, String end, String behavior){
        this.patienId = patienId;
        this.name = name;
        this.start = start;
        this.end = end;
        this.behavior = behavior;
    }

    public static ActivityDTO ofEntity(Activity activity){
        ActivityDTO dto = new ActivityDTO();
        dto.setId(activity.getId());
        dto.setPatienId(activity.getPatient().getPatientId());
        dto.setName(activity.getName());
        dto.setStart(activity.getStart().toString());
        dto.setEnd(activity.getEnd().toString());
        dto.setBehavior(activity.getBehavior());
        return dto;
    }
}
