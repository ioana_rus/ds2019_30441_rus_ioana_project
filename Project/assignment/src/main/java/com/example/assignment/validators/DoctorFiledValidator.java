package com.example.assignment.validators;

import com.example.assignment.dto.DoctorDTO;
import com.example.assignment.errorhandler.IncorrectParameterException;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
public class DoctorFiledValidator {
    private static final Log LOGGER = LogFactory.getLog(DoctorFiledValidator.class);

    public static void validateInsertOrUpdate(DoctorDTO doctorDTO){
        List<String> errors = new ArrayList<>();
        if(doctorDTO == null){
            errors.add("doctorDTO is null");
            throw new IncorrectParameterException(DoctorDTO.class.getSimpleName(), errors);
        }
        if(!errors.isEmpty()){
            LOGGER.error(errors);
            throw new IncorrectParameterException(DoctorFiledValidator.class.getSimpleName(), errors);
        }
    }
}
