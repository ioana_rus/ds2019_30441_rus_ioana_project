package com.example.assignment.validators;

import com.example.assignment.dto.MedicationDTO;
import com.example.assignment.errorhandler.IncorrectParameterException;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.ArrayList;
import java.util.List;

public class MedicationValidator {
    private static final Log LOGGER = LogFactory.getLog(MedicationValidator.class);
    public static  void validateInsertOrUpdate(MedicationDTO medicationDTO){
        List<String> errors = new ArrayList<>();
        if(medicationDTO == null){
            errors.add("medicationDTO is null");
            throw new IncorrectParameterException(MedicationDTO.class.getSimpleName(), errors);
        }
        if(!errors.isEmpty()){
            LOGGER.error(errors);
            throw new IncorrectParameterException(MedicationValidator.class.getSimpleName(), errors);
        }
    }
}
