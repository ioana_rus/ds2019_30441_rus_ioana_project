package com.example.assignment.services;

import com.example.assignment.dto.MedicationDTO;
import com.example.assignment.dto.builders.MedicationBuilder;
import com.example.assignment.entities.Medication;
import com.example.assignment.errorhandler.DuplicateEntryException;
import com.example.assignment.errorhandler.ResourceNotFoundException;
import com.example.assignment.repositories.MedicationRepository;
import com.example.assignment.validators.MedicationValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MedicationService {
    private final MedicationRepository medicationRepository;

    @Transactional
    public MedicationDTO findMedicationByName(String name){
        Optional<Medication> medication = medicationRepository.findByName(name);
        if(!medication.isPresent()){
            throw new ResourceNotFoundException("Medication","name",name);
        }
        return MedicationDTO.ofEntity(medication.get());
    }

    @Transactional
    public List<MedicationDTO> findAllMedications(){
        List<Medication> medications = medicationRepository.findAllOrdered();
        return medications.stream().map(MedicationDTO::ofEntity).collect(Collectors.toList());
    }

    @Transactional
    public Integer insertMedication(MedicationDTO medicationDTO){
        MedicationValidator.validateInsertOrUpdate(medicationDTO);
        return medicationRepository
                .save(MedicationBuilder.getEntityFromDTO(medicationDTO))
                .getMeidcationId();
    }

    @Transactional
    public Integer updateMedication(MedicationDTO medicationDTO){
        Optional<Medication> medication = medicationRepository.findById(medicationDTO.getMedicationId());
        if(!medication.isPresent()){
            throw new ResourceNotFoundException("Patient", "patient id", medicationDTO.getMedicationId());
        }
        MedicationValidator.validateInsertOrUpdate(medicationDTO);
        return medicationRepository.save(MedicationBuilder.getEntityFromDTO(medicationDTO)).getMeidcationId();
    }

    @Transactional
    public void deleteMedication(Integer id){
        this.medicationRepository.deleteById(id);
    }
}
