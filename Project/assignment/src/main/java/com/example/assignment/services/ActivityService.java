package com.example.assignment.services;

import com.example.assignment.dto.ActivityDTO;
import com.example.assignment.dto.builders.ActivityBuilder;
import com.example.assignment.entities.*;
import com.example.assignment.events.MessageRecievedEvent;
import com.example.assignment.repositories.ActivityRepository;
import com.example.assignment.repositories.MedicationPlanRepository;
import com.example.assignment.repositories.PatientRepository;
import com.example.assignment.repositories.RecommendationRepository;
import com.example.assignment.validators.ActivityValidator;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ActivityService {

    private final ActivityRepository activityRepository;
    private final ApplicationEventPublisher eventPublisher;
    private final ActivityValidator activityValidator;
    private final PatientRepository patientRepository;
    private final RecommendationRepository recommendationRepository;
    private final MedicationPlanRepository medicationPlanRepository;

    public Integer addActivity(ActivityDTO activityDTO){
        Activity activity = ActivityBuilder.getEntityFromDTO(activityDTO);
        String message = activityValidator.validateActivity(activity);
        if(!message.isEmpty()){
            System.out.println(message);
            eventPublisher.publishEvent(new MessageRecievedEvent(message));
            activity.setBehavior("not normal");
        }else{
            activity.setBehavior("normal");
        }
        return activityRepository.save(activity).getId();
    }

    @Transactional
    public ActivityDTO getActivity(Integer id){
        return ActivityDTO.ofEntity(activityRepository.findById(id).get());
    }

    @Transactional
    public List<ActivityData> getActivities(Integer id) {
        Patient patient = patientRepository.findById(id).get();
        List<Activity> activities = activityRepository.findAllByPatient(patient);
        List<ActivityData> data = new ArrayList<>();
        for(Activity activity:activities){
            Duration duration = Duration.between(activity.getStart(), activity.getEnd());
            long diff = Math.abs(duration.toMinutes());
            boolean check = true;
            for(ActivityData act:data){
                if(act.getName().equals(activity.getName())){
                    act.setDuration(act.getDuration()+diff);
                    check = false;
                }
            }
            if(check){
                data.add(new ActivityData(activity.getName(),diff));
            }
        }
        return data;
    }

    @Transactional
    public List<String> checkActivityBehavior(Integer patientId) {
        Patient patient = patientRepository.findById(patientId).get();
        List<Activity> activities = activityRepository.findAllByPatient(patient);
        List<String> abnormalBehavior = new ArrayList<>();
        for(Activity activity: activities){
            if(activity.getBehavior().equals("not normal")){
                abnormalBehavior.add(activity.getName());
            }
        }
        return abnormalBehavior;
    }

    public String checkPatientTookMedication(Integer patientId) {
        Patient patient = patientRepository.findById(patientId).get();
        MedicationPlan medicationPlan = medicationPlanRepository.findByPatient(patient).get();
        return medicationPlan.getStatus();
    }

    public void giveRecommendation(Integer id, String activity, String rec){
        Patient patient = patientRepository.findById(id).get();
        LocalDateTime date = LocalDateTime.now();
        Recommendation recommendation = new Recommendation(patient,rec,date,activity);
        recommendationRepository.save(recommendation);
    }
}
