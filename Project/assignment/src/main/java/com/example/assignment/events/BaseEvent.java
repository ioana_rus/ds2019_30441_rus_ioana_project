package com.example.assignment.events;

import lombok.Data;

@Data
public class BaseEvent {
    private final EventType type;
}
