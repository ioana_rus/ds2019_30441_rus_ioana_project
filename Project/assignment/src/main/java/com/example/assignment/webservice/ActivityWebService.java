package com.example.assignment.webservice;

import com.example.assignment.entities.ActivityData;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import java.util.List;

@WebService
@SOAPBinding(style = Style.DOCUMENT)
public interface ActivityWebService {

    @WebMethod
    List<ActivityData> getActivities(Integer id);

    @WebMethod
    String checkPatientTookMedication(Integer patientId);

    @WebMethod
    List<String> checkActivityBehavior(Integer patientId);

    @WebMethod
    void giveRecommendations(Integer patientId, String activity, String rec);
}
