package com.example.assignment.services;

import com.example.assignment.dto.MedicationPlanDTO;
import com.example.assignment.dto.builders.MedicationPlanBuilder;
import com.example.assignment.entities.Medication;
import com.example.assignment.entities.MedicationPlan;
import com.example.assignment.entities.Patient;
import com.example.assignment.errorhandler.ResourceNotFoundException;
import com.example.assignment.repositories.MedicationPlanRepository;
import com.example.assignment.repositories.MedicationRepository;
import com.example.assignment.repositories.PatientRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class MedicationPlanService {
    private final MedicationPlanRepository medicationPlanRepository;
    private final PatientRepository patientRepository;
    private final MedicationRepository medicationRepository;

    public MedicationPlanDTO getMedicationPlan(Integer patientId){
        Patient patient = patientRepository.findById(patientId).get();
        MedicationPlan medicationPlan = medicationPlanRepository.findByPatient(patient).get();
        System.out.println(medicationPlan.getMedictionPlanId());
        return MedicationPlanDTO.ofEntity(medicationPlan);
    }

    @Transactional
    public Integer updateMedicationPlan(Integer id, String newMed){
        MedicationPlan medicationPlan = medicationPlanRepository.findById(id).get();
        //if(!medication.isPresent()){
          //  throw new ResourceNotFoundException("MedicationPlan", "patient id", medicationPlanDTO.getId());
        //}
        //MedicationValidator.validateInsertOrUpdate(medicationDTO);
        //MedicationPlan medicationPlan = MedicationPlanBuilder.getEntityFromDTO(medicationPlanDTO);
        List<Medication> meds = medicationPlan.getMedication();
        //for(String name: medicationPlanDTO.getMedication()){
        //    Medication med = medicationRepository.findByName(name).get();
         //   meds.add(med);
        //}
        Medication med = medicationRepository.findByName(newMed).get();
        medicationPlan.setMedication(meds);
        return medicationPlanRepository.save(medicationPlan).getMedictionPlanId();
    }



}
