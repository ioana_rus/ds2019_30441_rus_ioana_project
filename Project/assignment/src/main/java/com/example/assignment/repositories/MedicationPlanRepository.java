package com.example.assignment.repositories;

import com.example.assignment.entities.MedicationPlan;
import com.example.assignment.entities.Patient;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface MedicationPlanRepository extends JpaRepository<MedicationPlan, Integer> {

    Optional<MedicationPlan> findByPatient(Patient patient);
}
