package com.example.assignment.dto;

import com.example.assignment.entities.Caregiver;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CaregiverDTO {
    private Integer caregiverId;
    private String name;
    private String gender;
    private String birthDate;
    private String address;
    private String username;
    private String password;

    public static CaregiverDTO ofEntity(Caregiver caregiver){
        CaregiverDTO dto = new CaregiverDTO();
        dto.setCaregiverId(caregiver.getCaregiverId());
        dto.setName(caregiver.getName());
        dto.setGender(caregiver.getGender());
        dto.setBirthDate(caregiver.getBirthDate().toString());
        dto.setAddress(caregiver.getAddress());
        dto.setUsername(caregiver.getUsername());
        dto.setPassword(caregiver.getPassword());
        return dto;
    }
}
