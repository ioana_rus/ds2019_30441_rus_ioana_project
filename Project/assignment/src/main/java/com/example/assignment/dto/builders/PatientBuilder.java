package com.example.assignment.dto.builders;

import com.example.assignment.dto.PatientDTO;
import com.example.assignment.entities.Caregiver;
import com.example.assignment.entities.Doctor;
import com.example.assignment.entities.Patient;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class PatientBuilder {
    public static Patient getEntityFromDTO(PatientDTO dto){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate birthDate = LocalDate.parse(dto.getBirthDate(), formatter);
        Doctor doctor = new Doctor();
        doctor.setDoctorId(dto.getDoctorId());
        Caregiver caregiver = new Caregiver();
        caregiver.setCaregiverId(dto.getCaregiverId());
        return new Patient(dto.getPatientId(),dto.getName(),dto.getGender(),birthDate,
                dto.getAddress(),dto.getUsername(),dto.getPassword(),doctor,caregiver,dto.getMedicalRecord());
    }
}
