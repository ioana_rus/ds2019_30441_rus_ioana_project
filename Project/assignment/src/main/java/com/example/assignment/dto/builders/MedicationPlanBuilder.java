package com.example.assignment.dto.builders;

import com.example.assignment.dto.MedicationPlanDTO;
import com.example.assignment.entities.*;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class MedicationPlanBuilder {
    public static MedicationPlan getEntityFromDTO(MedicationPlanDTO dto){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime startTime = LocalDateTime.parse(dto.getStartTime(), formatter);
        LocalDateTime endTime = LocalDateTime.parse(dto.getEndTime(),formatter);
        Patient patient = new Patient();
        patient.setPatientId(dto.getPatientId());
        List<Medication> meds = new ArrayList<>();

        return new MedicationPlan(dto.getId(),patient,startTime,endTime,meds,dto.getStatus());
    }
}
