package com.example.assignment.dto.builders;

import com.example.assignment.dto.ActivityDTO;
import com.example.assignment.entities.Activity;
import com.example.assignment.entities.Patient;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ActivityBuilder {
    public static Activity getEntityFromDTO(ActivityDTO dto){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime start = LocalDateTime.parse(dto.getStart(), formatter);
        LocalDateTime end = LocalDateTime.parse(dto.getEnd(),formatter);
        Patient patient = new Patient();
        patient.setPatientId(dto.getPatienId());
        return new Activity(dto.getId(),patient,dto.getName(),start,end,dto.getBehavior());
    }
}
