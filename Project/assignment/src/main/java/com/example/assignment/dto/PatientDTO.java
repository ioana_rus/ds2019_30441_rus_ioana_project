package com.example.assignment.dto;

import com.example.assignment.entities.Patient;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PatientDTO {
    private Integer patientId;
    private String name;
    private String gender;
    private String birthDate;
    private String address;
    private String username;
    private String password;
    private Integer doctorId;
    private Integer caregiverId;
    private String medicalRecord;

    public static PatientDTO ofEntity(Patient patient){
        PatientDTO patientDTO = new PatientDTO();
        patientDTO.setPatientId(patient.getPatientId());
        patientDTO.setName(patient.getName());
        patientDTO.setGender(patient.getGender());
        patientDTO.setBirthDate(patient.getBirthDate().toString());
        patientDTO.setAddress(patient.getAddress());
        patientDTO.setUsername(patient.getUsername());
        patientDTO.setPassword(patient.getPassword());
        patientDTO.setDoctorId(patient.getDoctor().getDoctorId());
        patientDTO.setCaregiverId(patient.getCaregiver().getCaregiverId());
        patientDTO.setMedicalRecord(patient.getMedicalRecord());
        return patientDTO;
    }
}
