package com.example.assignment.controllers;

import com.example.assignment.dto.CaregiverDTO;
import com.example.assignment.services.CaregiverService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class CaregiverController {
    private final CaregiverService caregiverService;

    @GetMapping("/caregivers/{id}")
    public CaregiverDTO findCaregiverById(@PathVariable Integer id){
        return caregiverService.findCaregiverById(id);
    }

    @GetMapping("/caregivers")
    public List<CaregiverDTO> findAllCaregivers(){
        return caregiverService.findAllCaregivers();
    }

    @PostMapping("/caregivers")
    public Integer insertCaregiver(@RequestBody CaregiverDTO caregiverDTO){
        return caregiverService.insertCaregiver(caregiverDTO);
    }

    @PutMapping("/caregivers")
    public Integer updateCaregiver(@RequestBody CaregiverDTO caregiverDTO){
        return caregiverService.updateCaregiver(caregiverDTO);
    }

    @DeleteMapping("/caregivers/{id}")
    public void deleteCaregiver(@PathVariable Integer id){
        caregiverService.deleteCaregiver(id);
    }

}
