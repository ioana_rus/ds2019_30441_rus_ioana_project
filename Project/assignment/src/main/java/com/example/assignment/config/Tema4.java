package com.example.assignment.config;

import com.example.assignment.webservice.ActivityWebServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.xml.ws.Endpoint;

@Component
public class Tema4 {
    @Autowired
    ActivityWebServiceImpl activityWebService;

    @PostConstruct
    public void addThis() {
        Endpoint.publish("http://localhost:7779/activityservice", activityWebService);

    }
}
