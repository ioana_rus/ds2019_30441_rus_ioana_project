package com.example.assignment.rmi;


import com.example.assignment.dto.MedicationPlanDTO;
import com.example.assignment.entities.Medication;
import com.example.assignment.entities.MedicationPlan;
import com.example.assignment.entities.Patient;
import com.example.assignment.repositories.MedicationPlanRepository;
import com.example.assignment.repositories.PatientRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Data
@AllArgsConstructor
public class MedicationPlanRMIimpl implements MedicationPlanRMI {
    private PatientRepository patientRepository;
    private MedicationPlanRepository medicationPlanRepository;

    @Override
    @Transactional
    public MedicationPlanDTO displayMedicationPlan(String patientId) {
        Patient patient = patientRepository.findById(Integer.parseInt(patientId)).get();
        MedicationPlan medicationPlan =  medicationPlanRepository.findByPatient(patient).get();
        medicationPlan.setStatus("not yet");
        List<String> meds=new ArrayList<String>();
        for(Medication med: medicationPlan.getMedication()){
            meds.add(med.getName());
        }
        System.out.println("================Server Side ========================");
        System.out.println("Inside Rmi IMPL - Incoming msg : " + patientId);
        return MedicationPlanDTO.ofEntity(medicationPlan);
    }

    @Override
    public void medicationMessage(String message) {
        Patient patient = patientRepository.findById(1).get();
        MedicationPlan medicationPlan = medicationPlanRepository.findByPatient(patient).get();
        medicationPlan.setStatus(message);
        medicationPlanRepository.save(medicationPlan);
        System.out.println(message);
    }

}
