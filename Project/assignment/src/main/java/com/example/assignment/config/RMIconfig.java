package com.example.assignment.config;

import com.example.assignment.repositories.MedicationPlanRepository;
import com.example.assignment.repositories.PatientRepository;
import com.example.assignment.rmi.MedicationPlanRMI;
import com.example.assignment.rmi.MedicationPlanRMIimpl;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.remoting.rmi.RmiServiceExporter;
import org.springframework.remoting.support.RemoteExporter;

@Configuration
@RequiredArgsConstructor
public class RMIconfig {

    private final PatientRepository patientRepository;
    private final MedicationPlanRepository medicationPlanRepository;

    @Bean
    RemoteExporter registerRMIExporter(){

        RmiServiceExporter exporter = new RmiServiceExporter();
        exporter.setServiceName("medicationplanrmi");
        exporter.setServiceInterface(MedicationPlanRMI.class);
        exporter.setService(new MedicationPlanRMIimpl(patientRepository,medicationPlanRepository));
        exporter.setRegistryPort(1099);

        return exporter;
    }
}
