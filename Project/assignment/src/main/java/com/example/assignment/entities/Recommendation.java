package com.example.assignment.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.time.LocalDateTime;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "recommendation")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Recommendation {
    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;
    @ManyToOne()
    @JoinColumn(name = "patient_id", nullable = false)
    private Patient patient;
    @Column(name = "todo", nullable = false)
    private String toDo;
    @Column(name = "start_date", nullable = false)
    private LocalDateTime startDate;
    @Column(name = "activity", nullable = false)
    private String activity;

    public Recommendation(Patient patient,String toDo,LocalDateTime date, String activity){
        this.patient = patient;
        this.toDo = toDo;
        this.startDate = date;
        this.activity = activity;
    }
}

