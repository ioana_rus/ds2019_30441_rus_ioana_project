package com.example.assignment.seed;


import com.example.assignment.entities.*;
import com.example.assignment.repositories.DoctorRepository;
import com.example.assignment.repositories.MedicationPlanRepository;
import com.example.assignment.repositories.MedicationRepository;
import com.example.assignment.repositories.PatientRepository;
import com.example.assignment.services.ActivityService;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Component
@RequiredArgsConstructor
@Order(Ordered.HIGHEST_PRECEDENCE)
public class DoctorSeed implements CommandLineRunner {
    private final DoctorRepository doctorRepository;
    private final PasswordEncoder passwordEncoder;
    private final MedicationPlanRepository medicationPlanRepository;
    private final MedicationRepository medicationRepository;
    private final PatientRepository patientRepository;
    private final ActivityService activityService;

    @Override
    @Transactional
    public void run(String... args) throws Exception {

        if(doctorRepository.findAll().isEmpty()){
            String date = "1990-12-08";
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
            LocalDate birthDate = LocalDate.parse(date, formatter);
            doctorRepository.save(new Doctor("Ioana", "female", birthDate,"Str Ion Mester","ioana33" ,passwordEncoder.encode("ioana33")));
        }

        /*if(medicationPlanRepository.findAll().isEmpty()){
            LocalDateTime startDate = LocalDateTime.now();
            LocalDateTime endDate = LocalDateTime.now().plusMinutes(1);
            Medication med1 = medicationRepository.findByName("paracetamol").get();
            Medication med2 = medicationRepository.findByName("vitamina C").get();
            List<Medication> meds = new ArrayList<Medication>();
            meds.add(med1);
            meds.add(med2);
            Patient patient =  patientRepository.findById(5).get();
            medicationPlanRepository.save(new MedicationPlan(patient,startDate,endDate,meds));
        }*/

        /*List<ActivityData> activities = activityService.getActivities(1);
        for(ActivityData act:activities){
            System.out.println(act.getName()+" "+act.getDuration());
        }*/
        /*System.out.println("\n Behavior: \n");
        List<String> beh =  activityService.checkActivityBehavior(1);
        for(String str:beh){
            System.out.println(str);
        }*/


    }
}
