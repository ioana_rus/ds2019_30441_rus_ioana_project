package com.example.assignment.rmi;


import com.example.assignment.dto.MedicationPlanDTO;


public interface MedicationPlanRMI {
     MedicationPlanDTO displayMedicationPlan(String patientId);

     void medicationMessage(String message);
}
