package com.example.assignment.entities;

public enum Role {
    DOCTOR,
    PATIENT,
    CAREGIVER
}
