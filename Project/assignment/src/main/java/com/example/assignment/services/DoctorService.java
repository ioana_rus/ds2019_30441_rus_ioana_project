package com.example.assignment.services;

import com.example.assignment.dto.DoctorDTO;
import com.example.assignment.dto.builders.DoctorBuilder;
import com.example.assignment.entities.Doctor;
import com.example.assignment.errorhandler.ResourceNotFoundException;
import com.example.assignment.repositories.DoctorRepository;
import com.example.assignment.validators.DoctorFiledValidator;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class DoctorService implements UserDetailsService {
    private final DoctorRepository doctorRepository;

    @Transactional
    public DoctorDTO findDoctorById(Integer id){
        Optional<Doctor> doctor = doctorRepository.findById(id);
        if(!doctor.isPresent()){
            throw new ResourceNotFoundException("Doctor", "doctor id", id);
        }
        return DoctorDTO.ofEntity(doctor.get());
    }

    @Transactional
    public Integer updateDoctor(DoctorDTO doctorDTO){
        Optional<Doctor> doctor = doctorRepository.findById(doctorDTO.getDoctorId());
        if(!doctor.isPresent()){
            throw new ResourceNotFoundException("Doctor", "doctor id", doctorDTO.getDoctorId());
        }
        DoctorFiledValidator.validateInsertOrUpdate(doctorDTO);
        return doctorRepository.save(DoctorBuilder.getEntityFromDTO(doctorDTO)).getDoctorId();
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Doctor doctor = doctorRepository.findByUsername(username).orElseThrow(()->new UsernameNotFoundException("Unknown user!"));
        System.out.println(doctor.getUsername()+" "+doctor.getPassword());
        return new org.springframework.security.core.userdetails.User(doctor.getUsername(),doctor.getPassword(),
                Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER")));
    }
}